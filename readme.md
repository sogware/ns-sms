## Laravel code example

**Korte omschrijving van het project:**  
Een gebruiker die elke dag hetzelfde traject aflegt per trein kan zich aanmelden voor een SMS waarschuwing waarin staat of de trein vertraging heeft of niet.

**Gebruikte technieken:**  
- BizzSMS API  
- NS API  
- Laravel  
- SQLite  
  
**Aangepast:**  
app/Console/Kernel.php  
app/Http/routes.php  
composer.json  
config/app.php  
config/database.php  
  
**Aangemaakt:**  
app/Console/Commands/SendAlarms.php  
app/Http/Controllers/AboutController.php  
app/Http/Controllers/RegisterController.php  
app/Http/Controllers/SearchController.php  
app/Http/Requests/RegisterFormRequest.php  
app/Alarm.php  
app/Classes/\*  
config/nsms.php  
database/migrations/\*  
public/css/\*  
public/js/\*  
resources/views/layout/\*  
resources/views/pages/\*  
storage/database.sqlite  
storage/ns-api-stations-v2.xml