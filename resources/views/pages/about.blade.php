@extends('layout.default')
@section('content')
<article>
    <header>
        <h1>NS SMS App</h1>
    </header>
    <section>
        <div class="ui-widget">
        {!! Form::open(['action' => ['SearchController@autocomplete'], 'method' => 'GET']) !!}
            {!! Form::text('origin', '', ['id' =>  'origin', 'placeholder' =>  'Vertrek station']) !!}<br />
            <br />
            {!! Form::text('destination', '', ['id' =>  'destination', 'placeholder' =>  'Aankomst station']) !!}
        {!! Form::close() !!}
        </div>
    </section>
    <section>
        <a href="{!! url('/') !!}">back</a>
    </section>
</article>
<script>
$(function() {
    $( "#origin" ).autocomplete({
        source: "search/autocomplete",
        minLength: 3,
        select: function(event, ui) {
            event.preventDefault();
            $('#origin').val(ui.item.value);
        }
    });
    $( "#destination" ).autocomplete({
        source: "search/autocomplete",
        minLength: 3,
        select: function(event, ui) {
            event.preventDefault();
            $('#destination').val(ui.item.value);
        }
    });
	
});
</script>
@stop