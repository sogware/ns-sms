@extends('layout.default')
@section('content')
<article>
    <header>
        <h1>Ontvang een NS SMS</h1>
        <p>Reis je elke dag hetzelfde traject? Ontvang dan een SMS met daarin de verwachte vertrektijd!</p>
    </header>
    <section>
        <h2>Voer je 06 nummer in</h2>
        <p>Je ontvangt een code die je hieronder moet invullen</p>
    </section>
</article>
@stop