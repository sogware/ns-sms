@extends('layout.default')
@section('content')

{!! Form::open(array('route' => 'overview', 'class' => 'form')) !!}


<article>
    <header>
    </header>
    <section>
        <h2>Stap 3</h2>
        <p>Voer de reisgegevens in</p>
        {!! Form::label('origin', 'Vertrek station') !!}
        {!! Form::text('origin', '', ['id' =>  'origin', 'placeholder' =>  'Vertrek station']) !!}<br />
        
        {!! Form::label('destination', 'Aankomst station') !!}
        {!! Form::text('destination', '', ['id' =>  'destination', 'placeholder' =>  'Aankomst station']) !!}<br />
        
        {!! Form::label('depart', 'Vertrek tijd') !!}
        {!! Form::input('number', 'departHour', '', ['placeholder' => 'UU', 'class' => 'number']) !!}&nbsp;:&nbsp;
        {!! Form::input('number', 'departMinute', '', ['placeholder' => 'MM', 'class' => 'number']) !!}<br />
        
        {!! Form::label('daytype', 'Dag van de week') !!}
        {!! Form::select('daytype', array(0 => 'Werkdagen', 5 => 'Zaterdag', 6 => 'Zondag')) !!}<br />
        
        {!! Form::label('offset', 'Waarschuwing') !!}
        {!! Form::input('number', 'offset', '10', ['placeholder' => 'MM', 'class' => 'number']) !!}&nbsp;minuten voor vertrek
    </section>
    <section>
        {!! Form::submit('OK', array('class'=>'btn btn-primary')) !!}
    </section>
</article>

<script>
$(function() {
    $( "#origin" ).autocomplete({
        source: "/search/autocomplete",
        minLength: 3,
        select: function(event, ui) {
            event.preventDefault();
            $('#origin').val(ui.item.value);
        }
    });
    $( "#destination" ).autocomplete({
        source: "/search/autocomplete",
        minLength: 3,
        select: function(event, ui) {
            event.preventDefault();
            $('#destination').val(ui.item.value);
        }
    });
	
});
</script>

{!! Form::close() !!}

@stop