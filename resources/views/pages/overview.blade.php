@extends('layout.default')
@section('content')

<article>
    <header>
    </header>
    <section>
        <h2>Overzicht</h2>
        <p>Wij hebben de volgende reis voor je gevonden:</p>
        Van <b>{!! $alarm->origin !!}</b> 
        naar <b>{!! $alarm->destination !!}</b> 
        op <b>
           @if ($alarm->daytype == 0)
                werkdagen
           @elseif ($alarm->daytype == 5)
                zaterdagen
           @elseif ($alarm->daytype == 6)
                zondagen
           @endif
           </b> 
        rond <b>{!! $alarm->prefDeparture !!}</b>.<br />
    </section>
    <section>
        <table class="styleTable">
            <tr><th>Vertrek</th><th>Spoor</th><th>Reistijd</th><th>Aankomst</th><th>Type</th></tr>
            <tr>
                <td>{!! $trip->departure !!}</td>
                <td>{!! $trip->ReisDeel->ReisStop[0]->Spoor !!}</td>
                <td>{!! $trip->GeplandeReisTijd !!}</td>
                <td>{!! $trip->arrival !!}</td>
                <td>{!! $trip->ReisDeel->VervoerType !!}</td>
            </tr>
        </table>
    </section>
    <section>
        {!! Form::open(array('route' => 'store', 'class' => 'form')) !!}
        {!! Form::submit('OK', array('class'=>'btn btn-primary')) !!}
        {!! Form::close() !!}
    </section>
</article>

@stop