@extends('layout.default')
@section('content')

<article>
    <header>
    </header>
    <section>
        <h2>Voltooid</h2>
        <p>Hartelijk dank voor het gebruik van NS-SMS</p>
        <p>Wij hebben de volgende reis voor je opgeslagen:</p>
    </section>
    <section>
        Van <b>{!! $alarm->origin !!}</b> 
        naar <b>{!! $alarm->destination !!}</b> 
        op <b>
           @if ($alarm->daytype == 0)
                werkdagen
           @elseif ($alarm->daytype == 5)
                zaterdagen
           @elseif ($alarm->daytype == 6)
                zondagen
           @endif
           </b> 
        om <b>{!! $alarm->departure !!}</b>.<br />
        Wij sturen je <b>{!! $alarm->offset !!} minuten</b> voor de geplande vertrektijd een SMS naar <b>{!! $alarm->phone !!}</b>
    </section>
    <section>
        {!! link_to_route('register', 'Plan nog een reis') !!}
    </section>
</article>

@stop