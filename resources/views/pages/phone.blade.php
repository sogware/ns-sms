@extends('layout.default')
@section('content')

{!! Form::open(array('route' => 'code', 'class' => 'form')) !!}

<article>
    <header>
        <p>Reis je elke dag hetzelfde traject? Ontvang dan een SMS met daarin de verwachte vertrektijd!</p>
    </header>
    <section>
        <h2>Stap 1</h2>
        <p>Voer je nummer in</p>
        <p>Je ontvangt een code die je in Stap 2 moet invullen</p>
        {!! Form::label('Mobiel nummer') !!}
        {!! Form::text('phone', null, 
            array('required', 
                  'class'=>'form-control', 
                  'placeholder'=>'0031612345678')) !!}
    </section>
    <section>
        {!! Form::submit('OK', array('class'=>'btn btn-primary')) !!}
    </section>
    
</article>


{!! Form::close() !!}

@stop
