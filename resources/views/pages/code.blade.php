@extends('layout.default')
@section('content')
@if (isset($error))
<p>{{ $error }}</p>
@endif

{!! Form::open(array('route' => 'trip', 'class' => 'form')) !!}

<article>
    <header>
    </header>
    <section>
        <h2>Stap 2</h2>
        <p>Voer de ontvangen code in (DEBUG: {!! $code !!})</p>
        <p>DEBUG: {!! $result !!}</p>
        {!! Form::label('Code') !!}
        {!! Form::text('code', null, 
            array('required', 
                  'class'=>'form-control', 
                  'placeholder'=>'123456')) !!}
    </section>
    <section>
        {!! Form::submit('OK', array('class'=>'btn btn-primary')) !!}
    </section>
</article>

{!! Form::close() !!}

@stop
