<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Alarm;
use Carbon;
use Log;
use App\Classes\NSAPI;
use App\Classes\BizzSMS;

class SendAlarms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alarm:send 
                                {--daytype= : 0=weekday; 5=Saturday; 6=Sunday}
                                {--time= : HH:MM}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Alarms';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $daytype = $this->option('daytype');
        $time = $this->option('time');

        // get current date and time
        $dt = Carbon::create();
        if (!isset($daytype)) {
            switch ($dt->dayOfWeek) {
                case 0: // Sunday
                    $daytype = 6; break;
                case 6: // Saturday
                    $daytype = 5; break;
                default: // workdays
                    $daytype = 0; break;
            }
        }
        if (!isset($time)) {
            $time = $dt->format('H:i');
        }
        
        $alarms = Alarm::where('daytype', $daytype)->where('warning', $time)->get();
        foreach ($alarms as $alarm) {
            // get info from nsapi
            $trip = NSAPI::getTrip($alarm);
            switch ($trip->Status) {
                case 'VOLGENS-PLAN': $alarm->status = 'Op tijd'; break;
                case 'GEWIJZIGD': $alarm->status = 'Gewijzigd'; break;
                case 'VERTRAAGD': $alarm->status = 'Vertraagd'; break;
                case 'NIEUW': $alarm->status = 'Nieuw'; break;
                case 'NIET-OPTIMAAL': $alarm->status = 'Niet optimaal'; break;
                case 'NIET-MOGELIJK': $alarm->status = 'Niet mogelijk'; break;
                case 'PLAN-GEWIJZIGD': $alarm->status = 'Plan gewijzigd'; break;
            }
            if (isset($trip->VertrekVertraging)) {
                $alarm->status .= ": $trip->VertrekVertraging minuten";
            }
            $body  = "Van: $alarm->origin\n";
            $body .= "Naar: $alarm->destination\n";
            $body .= "Status: $alarm->status\n";
            $body .= "Afmelden: $alarm->code\n";
            $result = BizzSMS::sendSMS($alarm->phone, $body, $alarm->code);
            $this->info($result);
            \Log::info("Send SMS to $alarm->phone for code $alarm->code @ " . \Carbon\Carbon::now());
        }
        
        if (count($alarms) == 0) {
            \Log::info("Nothing to do @ " . \Carbon\Carbon::now());
        }

    }
}
