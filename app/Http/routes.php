<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Alarm;

Route::get('/', function () {
    return redirect()->route('register');
});

// register steps
Route::get('register', ['as' => 'register', 'uses' => 'RegisterController@phone']);
Route::get('register/index', ['as' => 'index', 'uses' => 'RegisterController@index']);
Route::post('register/code', ['as' => 'code', 'uses' => 'RegisterController@code']);
Route::post('register/trip', ['as' => 'trip', 'uses' => 'RegisterController@trip']);
Route::post('register/overview', ['as' => 'overview', 'uses' => 'RegisterController@overview']);
Route::post('register/store', ['as' => 'store', 'uses' => 'RegisterController@store']);

Route::controller('about', 'AboutController');

// used for jQuery Autocomplete of Station names
Route::get('search/autocomplete', 'SearchController@autocomplete');

