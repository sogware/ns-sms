<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{

    public function index()
    {
        return false;
    }

    public function autocomplete($term = '')
    {
    
        $term = \Input::get('term');
        
    	$results = array();
    	
    	$xml = simplexml_load_file('../storage/ns-api-stations-v2.xml');
#    	$xml = simplexml_load_file('../storage/ns-test.xml');

		$results = array();
		foreach ($xml as $xmlStation) {
		
		  $names = [
		      (string)$xmlStation->Namen->Kort,
		      (string)$xmlStation->Namen->Middel,
		      (string)$xmlStation->Namen->Lang,
		  ];
		  
		  if (preg_grep("/^$term/i", $names)) {
		      $name = (string)$xmlStation->Namen->Lang;
		      $code = (string)$xmlStation->Code;
		      $country = (string)$xmlStation->Land;
		      //$station = ['name' => $name, 'code' => $code, 'country' => $country];
		      $result[] = [ 'code' => $code, 'value' => $name, 'country' => $country ];
		  }
		}
		
		if (isset($result)) {
    		return response()->json($result);
        } else {
            return '';
        }
		
    }

}
