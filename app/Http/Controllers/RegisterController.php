<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes\NSAPI;
use App\Classes\BizzSMS;
use App\Alarm;
use Carbon\Carbon;

class RegisterController extends Controller
{
	
	public function index() {
	   
	   $alarms = Alarm::all();
	
	   return $alarms;
	    
	}
	
    public function phone(Request $request) {

	   return view('pages.phone');
	    
	}
	
    public function code(Request $request) {

        $this->validate($request, [
            'phone' => 'required|numeric',
        ]);

	   // get a 6 digit random code     	
	   $digits = 6;
	   $code1 = rand(pow(10, $digits-1), pow(10, $digits)-1);
	   
	   // store things in a session
	   $request->session()->put('code1', $code1);
	   $request->session()->put('phone', $request->get('phone'));
	   
	   // send code SMS to phone
	   $body  = "Uw verificatiecode voor NS SMS:\n";
	   $body .= $code1;
	   $result = BizzSMS::sendSMS($request->get('phone'), $body, $code1);
	   
	   return view('pages.code', ['code' => $code1, 'result' => $result]);
	   
	}
	
	public function trip(Request $request) {
	   
	   $phone = $request->session()->get('phone');
	   $code1 = $request->session()->get('code1');
	   $code2 = $request->input('code');
	   
	   // check code
	   if ($code2 == $code1) {
    	   return view('pages.trip');
	   } else {
    	   return view('pages.code', ['code' => $code1, 'result' => '', 'error' => 'Foute code']);
	   }   

	}
	
	public function overview(Request $request) {
	   
	   $input = $request->all();
	   $input['departure'] = $input['departHour'] . ':' . $input['departMinute'];
	   $input['phone'] = $request->session()->get('phone');
	   $input['code'] = $request->session()->get('code1');
	   
	   $alarm = new Alarm($input);
	   $trip = NSAPI::getTrip($alarm);
	   $alarm->prefDeparture = $alarm->departure;
	   $alarm->departure = (string)$trip->departure;

	   $request->session()->put('alarm', $alarm);
	   
	   return view('pages.overview', ['alarm' => $alarm, 'trip' => $trip]);

	}
	
	public function store(Request $request) {
	   
	   $alarm = $request->session()->get('alarm');
	   unset($alarm->prefDeparture);
       $carbon = new Carbon($alarm->departure);
       $alarm->warning = $carbon->subMinutes($alarm->offset)->format('H:i');
	   
	   $alarm->save();
	   
	   return view('pages.store', ['alarm' => $alarm]);

	}
	
}
