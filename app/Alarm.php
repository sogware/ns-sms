<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alarm extends Model
{
    
    protected $fillable = [
        'phone',
        'code',
        'origin',
        'destination',
        'departure',
        'warning',
        'offset',
        'daytype'
    ];
    
}
