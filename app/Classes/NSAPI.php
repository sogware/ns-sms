<?php 

namespace App\Classes;

use App\Alarm;
 
class NSAPI{

	private static function getUrl($endpoint, $vars = array()) 
	{
	
		// write query string
		$query = '?' . http_build_query($vars);
		$url = config('nsms.ns_url') . $endpoint . $query;

		// create authentication context
		$token = config('nsms.ns_user').':'.config('nsms.ns_pass');
		$context = stream_context_create(array(
			'http' => array(
				'header'  => "Authorization: Basic " . base64_encode($token)
			)
		));
		
		// get data
		$data = file_get_contents($url, false, $context);
		if (!$data) {
			return false;
        }
		
		// return result as XML
		$xmlTree = simplexml_load_string($data);
		
		return $xmlTree;
		
	}
	
	public static function getTrip($alarm)
	{
	
	    // construct the next date according to daytype
	    $date = new \DateTime(null, new \DateTimeZone('Europe/Amsterdam'));
	    switch ($alarm->daytype) {
	       case 0:
	           $date->setTimestamp(strtotime('next monday')); break;
	       case 5:
	           $date->setTimestamp(strtotime('next saturday')); break;
	       case 6:
	           $date->setTimestamp(strtotime('next sunday')); break;
	    }
	    $departure = explode(':', $alarm->departure);
	    $date->setTime($departure[0], $departure[1]);
	    
    	$options = [
    	   'fromStation' => $alarm->origin,
    	   'toStation' => $alarm->destination,
    	   'dateTime' => $date->format('c')
        ];
		$xmlTree = self::getUrl('ns-api-treinplanner', $options); 

        // get the optimal route
		$trip = $xmlTree->xpath('/ReisMogelijkheden/ReisMogelijkheid[Optimaal="true"]')[0];
		
		// add some extra attributes
		$date = new \DateTime($trip->GeplandeVertrekTijd);
		$trip->departure = $date->format('H:i');
		$date = new \DateTime($trip->GeplandeAankomstTijd);
		$trip->arrival = $date->format('H:i');
		
		
		return $trip;
		
	}

}
