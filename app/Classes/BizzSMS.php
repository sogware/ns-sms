<?php 

namespace App\Classes;

use GuzzleHttp\Client;

class BizzSMS{

    private static function buildURL($recipient, $message, $reference) {

        $username       = config('nsms.bizzsms_username');
        $code           = config('nsms.bizzsms_code');
        $sendertitle    = config('nsms.bizzsms_sendertitle');
        $message        = urlencode($message);
        
        $url = "http://klanten.bizzsms.nl/api/send?username=$username&code=$code&text=$message&phonenumbers=$recipient&sendertitle=$sendertitle";
    
        return $url;

    }
    
    public static function sendSMS($recipient, $message, $reference=null) {
    
        $url = self::buildURL($recipient, $message, $reference);
        
        $client = new Client();
        $response = $client->get($url);
        $body = (string)$response->getBody();
        if ($body == '1|0|Message added to queue') { // OK
            return true;
        } else {
            return $body;
        }

    }
    
}
